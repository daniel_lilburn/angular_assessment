import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { starWarsAPIService } from './star-wars-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  //declare models for this component
  users:Object[] = []
  frmId:number = 1
  choices:Object[] = [{cat:'people'},{cat:'planets'},{cat:'vehicles'},{cat:'species'},{cat:'starships'}]
  whichCat:string  = 'people'
  model //class needed to remove browser errors

  //declare funcitons
  constructor(private placeholderService:starWarsAPIService){}

  handleClick(){
    //invoke the services
    this.placeholderService.getParaData(this.frmId, this.whichCat)
      .subscribe( (result)=>{this.model = result})
      var div_view = document.getElementById("viewMode");
      div_view.className = "visible"
  }

  //call method from our service
  invokeServicee(){
    this.placeholderService.getData().subscribe( (result)=>{
      this.users = result
      console.log(result)})
      
  }

  ngOnInit(){
    //make a call for user data
    this.invokeServicee()
    var div_view = document.getElementById("viewMode");
    div_view.className = "invisible"
  }
}