import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class starWarsAPIService {

  apiUrl:string = 'https://swapi.co/api/'

  constructor(private http:HttpClient) { }

  //declare methods of service
  getData():Observable<Object[]>{
    return this.http.get<Object[]>(`${this.apiUrl}people`)
    .pipe(
      catchError(this.handleError<Object[]>('getData', []))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      alert("id not found")
      // Let the app keep running by returning an empty result.
      return throwError("Something went wrong, try again later");
    };
  }

  getParaData(id, category){
    return this.http.get<Object[]>(`${this.apiUrl}${category}/${id}`)
      .pipe(
        catchError(this.handleError<Object[]>('getParaData', []))
      );
  }
}