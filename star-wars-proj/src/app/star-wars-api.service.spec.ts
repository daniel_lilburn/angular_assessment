import { TestBed } from '@angular/core/testing';

import { StarWarsAPIService } from './star-wars-api.service';

describe('StarWarsAPIService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StarWarsAPIService = TestBed.get(StarWarsAPIService);
    expect(service).toBeTruthy();
  });
});
